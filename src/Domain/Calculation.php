<?php

namespace App\Domain;

/**
 * Value Object for holding the result of a calculation
 */
final class Calculation
{

    /** @var float */
    private $result;

    /** @var Rate */
    private $response;

    public function __construct(float $result = 0, Rate $response = null)
    {
        $this->result = $result;
        $this->response = $response ?? Rate::APPROVED();
    }

    public function __toString()
    {
        if ($this->response == Rate::APPROVED()) {
            return (string) $this->result;
        }

        return (string) $this->response;
    }

    public function getResult(): float
    {
        return $this->result;
    }

    public function getResponse(): Rate
    {
        return $this->response;
    }

    public function add(float $amount): Calculation
    {
        return new Calculation($this->result + $amount, $this->response);
    }

    public function subtract(float $amount): Calculation
    {
        return new Calculation($this->result - $amount, $this->response);
    }

    public function multiply(float $amount): Calculation
    {
        return new Calculation($this->result * $amount, $this->response);
    }
    
    public function divide(float $amount): Calculation
    {
        return new Calculation($this->result / $amount, $this->response);
    }
}