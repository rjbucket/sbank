<?php

namespace App\Entity\Embeddable;

use App\Entity\Embeddable\PersonName;
// use App\Entity\Embeddable\ContactInfo;
use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ranjan Kumar Barik <ranjandee1@gmail.com>
 * @ORM\Embeddable
 */
class Person
{

    /**
     * @var PersonName
     * @Assert\Valid
     * @ORM\Embedded(class="App\Entity\Embeddable\PersonName")
     */
    protected $name;

    /**
     * @var DateTime
     * @ORM\Column(type="date", nullable=true)
     * @Assert\LessThan(
     *   value = "-18 years",
     *   message = "Person must be at least 18 years old"
     * )
     */
    protected $dob;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex("/^[a-z\s\'\-]*$/mi")
     * @Assert\Length(max=64)
     */
    protected $title;

    /**
     * @var ContactInfo
     * @Assert\Valid
     * @ORM\Embedded(class="App\Entity\Embeddable\ContactInfo")
     */
    protected $contact;

    public function __construct()
    {
        $this->name = new PersonName();
        $this->contact = new ContactInfo();
    }

    public function __toString()
    {
        return (string) $this->getName();
    }

    /** @return PersonName */
    public function getName()
    {
        return $this->name;
    }
    /** @param PersonName $name @return self */
    public function setName(PersonName $name)
    {
        $this->name = $name;
        return $this;
    }
    /** @return string */
    public function getTitle()
    {
        return $this->title;
    }
    /** @param string $title @return self */
    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }
    /** @return ContactInfo */
    public function getContact()
    {
        return $this->contact;
    }
    /** @param ContactInfo $contact @return self */
    public function setContact(ContactInfo $contact)
    {
        $this->contact = $contact;
        return $this;
    }
    /** @return DateTime */
    public function getDob()
    {
        return $this->dob;
    }
    /** @param DateTime $dob @return self */
    public function setDob(?DateTime $dob)
    {
        $this->dob = $dob;
        return $this;
    }
}
