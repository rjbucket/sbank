<?php

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ranjan Kumar Barik <ranjandee1@gmail.com>
 * @ORM\Embeddable
 */
final class Address
{

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     * @Assert\Regex("/^[\w\s\'\-\.\#]+$/mi")
     */
    private $address1;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     * @Assert\Regex("/^[\w\s\'\-\.\#]+$/mi")
     */
    private $address2;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     * @Assert\Regex(
     *  value = "/^[a-zA-Z]+$/mi",
     *  message = "City can only have Letters"
     * )
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     */
    private $state;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=10)
     * @Assert\Regex(
     *   value = "/^[0-9a-zA-Z]+$/mi",
     *   message = "Zip can only have Letters/Numbers"
     * )
     */
    private $zip;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     * @Assert\Regex(
     *   value = "/^[a-zA-Z ]+$/mi",
     *   message = "Country can only have Letters"
     * )
     */
    private $county;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex("/^[\w\s\'\-]+$/mi")
     */
    private $country = "United States";


    /**
     * @return string
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param string $address1
     *
     * @return self
     */
    public function setAddress1(string $address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param string $address2
     *
     * @return self
     */
    public function setAddress2(?string $address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity(string $city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     *
     * @return self
     */
    public function setState(string $state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     *
     * @return self
     */
    public function setZip(string $zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * @return string
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param string $county
     *
     * @return self
     */
    public function setCounty(string $county)
    {
        $this->county = $county;

        return $this;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return self
     */
    public function setCountry(string $country)
    {
        $this->country = $country;

        return $this;
    }
}
