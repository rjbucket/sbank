<?php

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ranjan Kumar Barik <ranjandee1@gmail.com>
 * @ORM\Embeddable
 */
final class PersonName
{

    /**
     * @var string
     * @Assert\Regex(
     *   value = "/^[a-z\s\'\-]*$/mi",
     *   message = "Name can only contain letters, apostrophes, spaces, and dashes."
     * )
     * @Assert\Length(max = 64)
     * @ORM\Column(type="string", nullable=true)
     */
    private $first = "";

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max = 64)
     * @Assert\Regex(
     *   value = "/^[a-z\s\'\-]*$/mi",
     *   message = "Name can only contain letters, apostrophes, spaces, and dashes."
     * )
     */
    private $middle = "";

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max = 64)
     * @Assert\Regex(
     *   value = "/^[a-z\s\'\-]*$/mi",
     *   message = "Name can only contain letters, apostrophes, spaces, and dashes."
     * )
     */
    private $last = "";

    /**
     * @param string[] $names
     * @param bool     $requireFullName
     */
    public function __construct(string ...$names)
    {
        // Clean up all the names
        array_walk($names, 'trim');

        // If no names provided, then treat this as a null object
        if (empty($names)) {
            $this->first = null;
            $this->middle = null;
            $this->last = null;
        }

        // If constructed with a single string, parse name by spaces
        if (count($names) === 1) {
            // Split up name by whitespace
            $names = explode(' ', $names[0]);

            // If, even after splitting the name, there's still only one name
            // Simply set it as the first name
            if (count($names) === 1) {
                $this->first = $names[0];
            }
        }

        // If there are two strings given, set first and last name
        if (count($names) === 2) {
            $this->first = $names[0];
            $this->last = $names[1];
        }

        // If there are more than two strings given, set first, middle, and last name
        else if (count($names) > 2) {
            $this->first = array_shift($names);
            $this->last = array_pop($names);
            $this->middle = implode(' ', $names);
        }
    }

    public function __toString()
    {
        return $this->getFullName() ?? "";
    }

    /**
     * Returns the full name in directory-style, with surname first.
     * e.g. "Colorado, Hexel James"
     *
     * @return string
     */
    public function getFullNameSurnameFirst()
    {
        // If there are at least a first and last name, follow requested format
        if (!empty($this->first) && !empty($this->last)) {
            return trim("{$this->last}, {$this->first} {$this->middle}");
        }

        // If there is a middle and last name but no first name (for whatever reason), continue with prescribed format
        if (!empty($this->middle) && !empty($this->last)) {
            return trim("{$this->last}, {$this->middle}");
        }

        // Otherwise, return straight full name, whatever it contains
        return $this->getFullName();
    }

    /**
     * Returns the full name in order, first to last
     * e.g. "Hexel James Colorado"
     *
     * @return string
     */
    public function getFullName()
    {
        $fullName = [];

        if (!empty($this->first)) {
            $fullName[] = $this->first;
        }

        if (!empty($this->middle)) {
            $fullName[] = $this->middle;
        }

        if (!empty($this->last)) {
            $fullName[] = $this->last;
        }

        if (count($fullName) > 0) {
            return implode(' ', $fullName);
        }

        return null;
    }

    /**
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @param string $first
     *
     * @return self
     */
    public function setFirst(string $first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * @return string
     */
    public function getMiddle()
    {
        return $this->middle;
    }

    /**
     * @param string $middle
     *
     * @return self
     */
    public function setMiddle(string $middle)
    {
        $this->middle = $middle;

        return $this;
    }

    /**
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * @param string $last
     *
     * @return self
     */
    public function setLast(string $last)
    {
        $this->last = $last;

        return $this;
    }
}
