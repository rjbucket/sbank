<?php

namespace App\Entity\Embeddable;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ranjan Kumar Barik <ranjandee1@gmail.com>
 * @ORM\Embeddable
 */
final class ContactInfo
{

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=64)
     * @Assert\Email(
     *   message = "This is not a valid email address"
     * )
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=20)
     * @Assert\Regex(
     *   value = "/^[0-9\-\s\(\)\,]+$/mi",
     *   message = "Please enter a valid phone number"
     * )
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(
     *   value = "/^[0-9\-\s\(\)\,]+$/mi",
     *   message = "Please enter a valid phone number"
     * )
     * @Assert\Length(max=64)
     */
    protected $cell;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(
     *   value = "/^[0-9\-\s\(\)\,]+$/mi",
     *   message = "Please enter a valid fax number"
     * )
     * @Assert\Length(max=20)
     */
    protected $fax;

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Length(max=255)
     */
    protected $website;

    /** @return string */
    public function getEmail()
    {
        return $this->email;
    }
    /** @return string */
    public function getPhone()
    {
        return $this->phone;
    }
    /** @return string */
    public function getFax()
    {
        return $this->fax;
    }
    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(?string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(?string $phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @param string $fax
     *
     * @return self
     */
    public function setFax(?string $fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * @return string
     */
    public function getCell()
    {
        return $this->cell;
    }

    /**
     * @param string $cell
     *
     * @return self
     */
    public function setCell($cell)
    {
        $this->cell = $cell;

        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     *
     * @return self
     */
    public function setWebsite(?string $website)
    {
        $this->website = $website;

        return $this;
    }
}
