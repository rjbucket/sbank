<?php

namespace App\Entity;

use App\Entity\Embeddable\Person;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Ranjan Kumar Barik <ranjandee1@gmail.com>
 * @ORM\Entity()
 * @ORM\Table(name="accounts")
 */
class Accounts extends Person
{

    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /** @var string @ORM\Column(type="string") */
    private $accountNumber;

    /** @var string @ORM\Column(type="string") */
    private $balance;

    /** @return int */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * @param string $accountNumber
     *
     * @return self
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getbalance()
    {
        return $this->balance;
    }

    /**
     * @param mixed $balance
     *
     * @return self
     */
    public function setbalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }
}
